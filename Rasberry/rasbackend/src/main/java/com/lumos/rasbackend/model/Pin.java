package com.lumos.rasbackend.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document("pins")
public class Pin {

    private int pinNo;
    private int status;

    public Pin() {

    }

    public int getPinNo() {
        return pinNo;
    }

    public void setPinNo(int pinNo) {
        this.pinNo = pinNo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
