package com.lumos.rasbackend.controller;

import com.lumos.rasbackend.model.Command;
import com.lumos.rasbackend.model.Pin;
import com.lumos.rasbackend.repository.CommandRepository;
import com.lumos.rasbackend.repository.PinRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RaspberryController{

    @Autowired
    private CommandRepository commandRepository;

    @Autowired
    private PinRepository pinRepository;

    @PostMapping("/komutEkle")
    private ResponseEntity addCommand(@RequestBody Command command){
        Command result = commandRepository.save(command);
        List<Pin> pinList = result.getPins();
        for (Pin pin : pinList) {
            pinRepository.save(pin);
        }
        if (result != null) {
            return new ResponseEntity(result, HttpStatus.OK);
        }
        else
            return new ResponseEntity("Command cannot be created", HttpStatus.BAD_REQUEST);
    }


    @GetMapping("/{commandName}")
    private ResponseEntity executeCommand(@RequestBody Command command){

        

    }

    @Query
    private Command findByCommandName(String commandName) {

        Command command = commandRepository.findByCommandName(commandName);
        return  command;
    }

}
